/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JasperReport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author developer
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            List productos  = new LinkedList();
            productos.add(new Producto("nombre", "22", "2", "44"));
            //All Files	C:\java\app_vyr\vyr2\src\theme\reparacion_matricial.jasper
            //file:///C:/app_vyr/theme/reparacion_matricial.jasper
            //URL ruta = new URL("file:///C:\\java\\app_vyr\\vyr2\\src\\theme\\reparacion_matricial.jasper");
            URL ruta = new URL("file:///C:/java/app_vyr/vyr2/src/theme/reparacion_matricial.jasper");
            
            //URL ruta = new URL("file:///C:/app_vyr/theme/reparacion_matricial.jasper");
            System.out.println(ruta.getFile());
            System.out.println(ruta.getPath());
            System.out.println(ruta.toString());
            //URL ruta = new URL("file:///C:/java/app_vyr/vyr2/src/theme/reparacion_matricial.jasper");

            //C:\java\app_vyr\vyr2\src\theme
            //URL ruta = new URL("file:///C:/java/app_vyr/vyr2/src/theme/reparacion_matricial.jrxml");
            
            //JasperReport loadObject = JasperCompileManager.compileReport("C:\\java\\app_vyr\\vyr2\\src\\theme\\reparacion_matricial.jrxml");
            //JasperReport loadObject = (JasperReport) JRLoader.loadObjectFromFile("C:\\java\\app_vyr\\vyr2\\src\\theme\\reparacion_matricial.jasper");
            JasperReport loadObject = (JasperReport) JRLoader.loadObject(ruta);
            //String s = "C:/app_vyr/theme/reparacion_matricial.jasper";
            //String s = "C:\\app_vyr\\theme\\reparacion_matricial.jasper";
            //JasperReport loadObject = (JasperReport) JRLoader.loadObjectFromFile(s);
            
            //JasperReport loadObject = JasperCompileManager.compileReport(s); // te falta esta linea

            //JasperReport loadObject = (JasperReport) JRLoader.loadObjectFromFile("C:/java/app_vyr/vyr2/src/theme/reparacion_matricial.jasper");
            //.loadObject()
           //JasperReport loadObject = (JasperReport) JRLoader.loadObject(getClass().getResource("../theme/Reparacion.jasper")/*"C:/vyr/theme/Reparacion.jasper"*/);
            Map parameters = new HashMap<String, Object>();
            JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(productos); 
            //System.out.println(getClass().getResource("../icono/iconoclinica.gif"));
            parameters.put("logo_empresa", Empresa.logo);
            parameters.put("nombre_empresa", Empresa.nombre);
            parameters.put("eslogan_empresa", Empresa.eslogan);

            parameters.put("ncf_empresa", "");
            parameters.put("rnc_empresa", Empresa.rnc);
            parameters.put("telefono_empresa", Empresa.telefono);
            parameters.put("celular_empresa", Empresa.celular);
            parameters.put("email_empresa", Empresa.email);            
            parameters.put("direccion_empresa", Empresa.direccion);
            
            parameters.put("abrebiatura_sucursal_empresa", "");
            parameters.put("clave_cliente", "");


            parameters.put("no_factura", "122345677");
            parameters.put("fecha", "");
                        
            parameters.put("rnc_cliente", "");
            parameters.put("numero_cliente", "");
            parameters.put("nombre_cliente", "");
            parameters.put("telefono_cliente", "");
            
            /*parameters.put("no_factura", this.idFactura);*/
            parameters.put("sub_total","$ ");
            parameters.put("monto_total","$ ");
            parameters.put("itbis", "$ ");
            parameters.put("nota", "");

            JasperPrint jp = JasperFillManager.fillReport(loadObject, parameters, ds);           
            JasperViewer jv = new JasperViewer(jp,false);
            jv.setDefaultCloseOperation(JasperViewer.DISPOSE_ON_CLOSE);
            jv.setVisible(true);
                    
        } catch (JRException ex) {
            System.out.println(ex.toString());
        } catch (MalformedURLException ex) {
            System.out.println(ex.toString());
        } 
    }
    
}
