/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author developer
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Eudy
 */
public class Empresa {
    public  static String nombre = "V & R";
    public  static String eslogan = "SERVICIOS DE HERRAMIENTAS";
    public  static String telefono = "809-285-3283/809-560-1142";
    //public final static String celular = "829-383-1914";    
    //public  static String celular = "809-719-2453/809-901-0111";
    //public  static String celular = "809-560-1142/809-901-1142";
    
    //public  static String celular = "809-901-0111";
    public  static String celular = "829-838-1914";
    public  static String email = "wilvalenz@hotmail.com @herramientasvyr";
    //public  static String direccion = "C/Padre Betancourt, Salida de los Alcarrizos, Santo Domingo Oeste, R.D.";
    public  static String direccion = "Calle San Antonio No. 35, Entrada De Los Alcarrizos, Santo Domingo Oeste, R.D.";
    public  static String web = "";
    //public final static String rnc = "01200969101";
    public  static String rnc = "132016149";
    public  static String logo = "C:/app_vyr/img/iconoclinica.gif";
    public  static String ncf = "B01";
    public static int tiempoEsperaEfectoBoton=1000;
    //public static String nombreSucursalSeleccionada = "VyR-Padre Bentacourt";
    //public static String abretiaturaSucursalSeleccionada = "VyR-PB";
    //public static String idSucursalSeleccionada = "1";
    public static String nombreSucursalSeleccionada = "VyR-San Antonio";
    public static String abretiaturaSucursalSeleccionada = "VyR-SA";
    public static String idSucursalSeleccionada = "2";
    
    public final static String[] direccionesSucursal = 
    {
        "C/Padre Betancourt, Salida de los Alcarrizos, Santo Domingo Oeste, R.D.",
        "Calle San Antonio No. 35, Entrada De Los Alcarrizos, Santo Domingo Oeste, R.D."
    };
    public final static String[] nombresSucursal = 
    {
        "VyR-Padre Bentacourt",
        "VyR-San Antonio"
    };
    public final static String[] abretiaturaSucursal = 
    {
        "VyR-PB",
        "VyR-SA"
    };
    public final static String[] user={"vyr","vyr"};
    public final static String[] pass={"123456","123456"};
    public final static String[] db_name={"db_vyr_cliente3_desktop","db_vyr_cliente3_sucursal_sa"};
    public final static String[] dbServidor={"pc","intranet","vyr.hopto.org"};
    public static void nombreSetLabel(JLabel label){
        label.setText(nombre);
    }
    public static  void logoSetIcon(JLabel label){
        label.setIcon(
                 new ImageIcon(
                         new ImageIcon("C:/app_vyr/img/iconoBlancoVYR.png")
                                 .getImage()
                                 .getScaledInstance(
                                         label.getWidth(), 
                                         label.getHeight(), 
                                         Image.SCALE_SMOOTH)));
        
    
    }
    public static void logoEmpresaAPP(javax.swing.JFrame frame){
        frame.setIconImage(new ImageIcon("C:/app_vyr/img/iconoclinica.gif").getImage());
        
    }
}
